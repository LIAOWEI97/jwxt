package com.bootstrap.jwxt.login.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
@ApiModel(value = "UserDto", description = "请求参数")
public class UserDto {
  @NotNull
  @ApiModelProperty(value = "用户名", example = "993859222")
  private String userName;
  @NotNull
  @ApiModelProperty(value = "密码", example = "3edc$RFV")
  private String password;
}
