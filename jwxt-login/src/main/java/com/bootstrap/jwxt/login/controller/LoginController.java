package com.bootstrap.jwxt.login.controller;

import com.bootstrap.jwxt.core.entity.UserEntity;
import com.bootstrap.jwxt.core.service.ILoginService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 用户登录 controller
 *
 * @date 2020/10/10
 * @author liaowei
 */
@RestController
@RequestMapping(value = "/users")
public class LoginController {

  private ILoginService iLoginService;

  @Autowired
  public void setiLoginService(ILoginService iLoginService) {
    this.iLoginService = iLoginService;
  }

  @GetMapping(value = "")
  @ApiOperation(value = "用户登录接口", notes = "说明", httpMethod = "GET")
  @ApiImplicitParams({
    @ApiImplicitParam(name = "userCode", value = "账号", required = true, dataType = "String"),
    @ApiImplicitParam(name = "password", value = "密码", required = true, dataType = "String"),
    @ApiImplicitParam(name = "verifyCode", value = "验证码", required = true, dataType = "String")
  })
  public Object addUser(@RequestParam String userCode,
                      @RequestParam String password,
                      @RequestParam String verifyCode) {
    System.out.println(userCode + " " + password);
    UserEntity user = iLoginService.userLogin(userCode, password, verifyCode);

    Map<String, Object> respose = new HashMap<>();
    respose.put("success", true);
    respose.put("data", user);
    return respose;
  }
}
