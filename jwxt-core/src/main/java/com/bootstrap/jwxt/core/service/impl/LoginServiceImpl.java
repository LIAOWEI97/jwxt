package com.bootstrap.jwxt.core.service.impl;

import com.bootstrap.jwxt.core.entity.UserEntity;
import com.bootstrap.jwxt.core.mapper.UserMapper;
import com.bootstrap.jwxt.core.service.ILoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.List;

/**
 * @date 2020/10/12
 * @author liaowei
 */
@Service
public class LoginServiceImpl implements ILoginService {
  private UserMapper userMapper;

  @Autowired
  public void setUserMapper(UserMapper userMapper) {
    this.userMapper = userMapper;
  }


  @Override
  public UserEntity userLogin(String userCode, String password, String verifyCode) {
    UserEntity user = userMapper.selectUser(userCode, password);
    Assert.notNull(user, "账号或密码错误!");
    return user;
  }
}
