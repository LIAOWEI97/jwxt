package com.bootstrap.jwxt.core.mapper;

import com.bootstrap.jwxt.core.entity.UserEntity;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @date 2020/10/12
 * @author liaowei
 */
public interface UserMapper {
  /**
   * 查找用户
   * @param userCode
   * @param password
   * @return
   */
    UserEntity selectUser(@Param("userCode") String userCode, @Param("password") String password);
}
