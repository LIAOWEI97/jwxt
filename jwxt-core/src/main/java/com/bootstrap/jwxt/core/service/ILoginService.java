package com.bootstrap.jwxt.core.service;

import com.bootstrap.jwxt.core.entity.UserEntity;

import java.util.List;

/**
 * 登录 service
 *
 * @date 2020/10/12
 * @author liaowei
 */
public interface ILoginService {
  /**
   * 用户登录
   * @param userCode
   * @param password
   * @param verifyCode
   * @return
   */
  UserEntity userLogin(String userCode, String password, String verifyCode);
}
