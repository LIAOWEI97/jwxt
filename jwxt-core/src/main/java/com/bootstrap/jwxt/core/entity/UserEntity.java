package com.bootstrap.jwxt.core.entity;

import lombok.Builder;
import lombok.Data;

import java.util.Date;

/**
 * 用户实体类
 *
 * @date 2020/10/12
 * @author liaowei
 */
@Data
@Builder
public class UserEntity {
  private Long id;
  private String userCode;
  private String idCard;
  private String homeAddress;
  private String name;
  private String password;
  private String userRole;
  private Date gmtCreate;
  private Date gmtModified;
}
